cmake_minimum_required(VERSION 3.12)
project(Doublons)

set(CMAKE_CXX_STANDARD 14)

if(CMAKE_CXX_COMPILER_ID MATCHES "GNU")
    add_compile_options(-Wall -Wpedantic)
endif()



add_executable(Doublons_iteratif src/iteratif/main.cpp src/generation.cpp src/utils/array_log.cpp src/iteratif/exec.cpp)

find_package(OpenMP REQUIRED)
add_executable(Doublons_openmp src/openmp/main.cpp src/openmp/exec.cpp src/generation.cpp src/utils/array_log.cpp)
target_link_libraries(Doublons_openmp PRIVATE OpenMP::OpenMP_CXX)

find_package(MPI REQUIRED)

include_directories(${MPI_INCLUDE_PATH})

add_executable(DoublonsMPI src/mpi/main.cpp src/mpi/mmpi/mmpi.cpp src/generation.cpp src/utils/repartition_algorithm.cpp)
target_link_libraries(DoublonsMPI ${MPI_LIBRARIES})
if (MPI_COMPILE_FLAGS)
    set_target_properties(DoublonsMPI PROPERTIES
            COMPILE_FLAGS "${MPI_COMPILE_FLAGS}")
endif ()

if (MPI_LINK_FLAGS)
    set_target_properties(DoublonsMPI PROPERTIES
            LINK_FLAGS "${MPI_LINK_FLAGS}")
endif ()
