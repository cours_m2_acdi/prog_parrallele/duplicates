//
// Created by etudiant on 26/11/18.
//

#ifndef DOUBLONS_OPENMP_EXEC_H
#define DOUBLONS_OPENMP_EXEC_H

#include <set>

void findDoubles(const int * matrix, int lineDim, int colDim, std::set<int>& duplicateIndexes);

#endif //DOUBLONS_OPENMP_EXEC_H
