#include <iostream>
#include <set>

#include "../generation.h"
#include "../utils/array_log.h"
#include "exec.h"

int main(int argc, char * argv[]) {

    int * matrix = new int[10*5];

    generateStatic(matrix, 10, 5);

    logMatrix(matrix, 10, 5, std::cout);

    std::set<int> duplicatesIndexes;

    findDoubles(matrix, 10, 5, duplicatesIndexes);

    for(auto & elt : duplicatesIndexes){
        std::cout << elt << " ";
    }
    std::cout << std::endl;

    std::cout << "==================" << std::endl;



    return 0;
}