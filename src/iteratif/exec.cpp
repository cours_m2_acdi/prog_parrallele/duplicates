//
// Created by etudiant on 26/11/18.
//
#include <iostream>
#include "exec.h"

bool areLinesEquals(const int * first, const int * second, int size){
    bool equal = true;
    for (int k = 0; k < size && equal; ++k) {
        if (first[k] != second[k]) {
            equal = false;
        }
    }
    return equal;
}

void findDoubles(const int * matrix, int lineDim, int colDim, std::set<int>& duplicateIndexes){
    for(int i = 0; i < lineDim; ++i) {
        if (duplicateIndexes.count(i) == 0) {
            for (int j = i + 1; j < lineDim; ++j) {
                if (areLinesEquals(matrix + i * colDim, matrix + j * colDim, colDim)) {
                    std::cout << "Duplicate "<< i << ", " << j << std::endl;
                    duplicateIndexes.insert(j);
                }
            }
        }
    }
}
