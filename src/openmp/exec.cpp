//
// Created by pviolette on 26/11/18.
//

#include <iostream>
#include <omp.h>
#include "exec.h"

bool areLinesEquals(const int * first, const int * second, int size){
    bool equal = true;
    for (int k = 0; k < size && equal; ++k) {
        if (first[k] != second[k]) {
            equal = false;
        }
    }
    return equal;
}

void findDoubles(const int * matrix, int lineDim, int colDim, std::set<int>& duplicateIndexes){
    omp_lock_t lock;
    omp_init_lock(&lock);
#pragma omp parallel for
    for(int i = 0; i < lineDim; ++i) {
        omp_set_lock(&lock);
        bool handle = duplicateIndexes.count(i) == 0;
        omp_unset_lock(&lock);

        if (handle) {
            for (int j = i + 1; j < lineDim; ++j) {
                if (areLinesEquals(matrix + i * colDim, matrix + j * colDim, colDim)) {
                    printf("Duplicate %d %d\n", i, j);

                    omp_set_lock(&lock);
                    duplicateIndexes.insert(j);
                    omp_unset_lock(&lock);
                }
            }
        }
    }
}
