//
// Created by pviolette on 26/11/18.
//

#include <random>
#include "generation.h"

void generateStatic(int * matrix, int lineDim, int colDim){
    for(int i = 0; i < lineDim; ++i){
        for(int j = 0; j < colDim; ++j){
            matrix[i * colDim + j] = j + i % 3;
        }
    }
}

void generateRandom(int *matrix, int lineDim, int colDim, int lowerBound, int uppedBound) {
    std::random_device rd;
    std::mt19937 mt(rd());
    std::uniform_int_distribution<int> dist(lowerBound, uppedBound);

    for(int i = 0; i < lineDim; ++i){
        for(int j = 0; j < colDim; ++j){
            matrix[i * colDim + j] = dist(mt);
        }
    }
}
