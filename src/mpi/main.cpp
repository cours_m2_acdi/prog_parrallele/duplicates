//
// Created by etudiant on 26/11/18.
//

#include <set>
#include "mmpi/mmpi.h"
#include "../generation.h"
#include "../utils/repartition_algorithm.h"

bool areLinesEquals(const int *first, const int *second, int size) {
    bool equal = true;
    for (int k = 0; k < size && equal; ++k) {
        if (first[k] != second[k]) {
            equal = false;
        }
    }
    return equal;
}


int main(int argc, char *argv[]) {

    MPI::Init(argc, argv);
    MMPI mmpi;

    int lineDim = 10;
    int colDim = 5;

    int *global_array;
    int start;
    int end;

    int opt;
    int upperBound = 10;
    bool verbose = false;
    while ((opt = getopt(argc, argv, "l:c:vu:")) != -1) {
        switch (opt) {
            case 'l':
                lineDim = atoi(optarg);
                break;
            case 'c':
                colDim = atoi(optarg);
                break;
            case 'v':
                verbose = true;
                break;
            case 'u':
                upperBound = atoi(optarg);
                break;
            default: /* aq?aq */
                fprintf(stderr, "Usage: %s [-s size]",
                        argv[0]);
                exit(EXIT_FAILURE);
        }
    }

    global_array = new int[lineDim * colDim];

    if (mmpi.is_master()) {

        generateRandom(global_array, lineDim, colDim, 1, upperBound);

        if(verbose){
            for(int i = 0; i < lineDim; ++i){
                mmpi << i << "=> [ ";
                for(int j = 0; j < colDim; ++j){
                    mmpi << global_array[i * colDim + j] << " ";
                }
                mmpi << "]" << MMPI::endl;
            }
        }

        std::vector<ThreadPosition> positions(static_cast<unsigned long>(mmpi.max_cpus()));
        equalRepartition(positions, lineDim, mmpi.max_cpus());

        start = positions[0].start;
        end = positions[0].end;

        for (int i = 1; i < mmpi.max_cpus(); ++i) {
            mmpi.remote(i);
            mmpi.send(global_array, lineDim * colDim);
            mmpi.send(positions[i].start);
            mmpi.send(positions[i].end);
        }

    } else {
        mmpi.remote(0);

        mmpi.recv(global_array, lineDim * colDim);

        mmpi.recv(start);
        mmpi.recv(end);
    }

    int * localDuplicates = new int[end - start];
    for(int i =0; i < end - start; ++i){
        localDuplicates[i] = -1;
    }
    int localDuplicatesCount = 0;

    for (int i = start; i < end; ++i) {
        bool isUnique = true;
        for (int j = 0; j < i && isUnique; ++j) {
            if (areLinesEquals(global_array + i * colDim, global_array + j * colDim, colDim)) {
                localDuplicates[localDuplicatesCount] = i;
                localDuplicatesCount++;
                isUnique = false;
            }
        }
    }

    if(verbose){
        mmpi << "Local duplicate ("<< localDuplicatesCount <<") =[";
        for(int i = 0; i < localDuplicatesCount; ++i){
            mmpi << localDuplicates[i] << " ";
        }
        mmpi << "]" << MMPI::endl;
    }

    MPI::COMM_WORLD.Barrier();

    if(mmpi.is_master()){
        int * countDuplicates = new int[mmpi.max_cpus()];

        countDuplicates[0] = localDuplicatesCount;

        int totalDuplicates = localDuplicatesCount;

        for(int i = 1; i < mmpi.max_cpus(); ++i){
            mmpi.remote(i),
            mmpi.recv(countDuplicates[i]);
            totalDuplicates += countDuplicates[i];
        }

        mmpi << "Duplicates count per thread = [";
        for(int i = 0; i < mmpi.max_cpus(); ++i){
            mmpi << countDuplicates[i] << " ";
        }
        mmpi << "]" << MMPI::endl;

        int * allDuplicates = new int[totalDuplicates];
        for(int i = 0; i < localDuplicatesCount; ++i){
            allDuplicates[i] = localDuplicates[i];
        }

        delete[] localDuplicates;

        int k = localDuplicatesCount;
        for(int i = 1; i < mmpi.max_cpus(); ++i){
            mmpi.remote(i);
            mmpi.recv(allDuplicates + k, countDuplicates[i]);
            k += countDuplicates[i];
        }

        if(verbose){
            mmpi << "duplicates= [";
            for(int i = 0; i < totalDuplicates; ++i){
                mmpi << allDuplicates[i] << ", ";
            }
            mmpi << "]" << MMPI::endl;
        }

        delete[] countDuplicates;
        delete[] allDuplicates;
    }else{
        mmpi.remote(0);
        mmpi.send(localDuplicatesCount);

        mmpi.send(localDuplicates, localDuplicatesCount);
        delete[] localDuplicates;
        delete[] global_array;
    }



    mmpi.finalize();
    MPI::Finalize();

    exit(EXIT_SUCCESS);
}