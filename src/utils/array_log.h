//
// Created by etudiant on 26/11/18.
//

#ifndef DOUBLONS_ARRAY_LOG_H
#define DOUBLONS_ARRAY_LOG_H

#include <ostream>

void logMatrix(const int * matrix, int lineDim, int colDim, std::ostream & out);
#endif //DOUBLONS_ARRAY_LOG_H
