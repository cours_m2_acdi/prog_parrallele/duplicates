//
// Created by pviolette on 26/11/18.
//

#include "array_log.h"

void logMatrix(const int *matrix, int lineDim, int colDim, std::ostream &out) {
    for(int i = 0; i <lineDim; ++i){
        for(int j = 0; j < colDim; ++j){
            out << matrix[i * colDim + j] << " ";
        }
        out << std::endl;
    }
}
