//
// Created by etudiant on 26/11/18.
//

#ifndef DOUBLONS_GENERATION_H
#define DOUBLONS_GENERATION_H

void generateStatic(int * matrix, int lineDim, int colDim);
void generateRandom(int * matrix, int lineDim, int colDim, int lowerBound = 0, int uppedBound = 10);

#endif //DOUBLONS_GENERATION_H
